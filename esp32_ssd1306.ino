

/************************************************************************************
 * BRANCHEMENT ESP32
 * SDA: GPIO 21
 * SCL: GPIO 22
 ***********************************************************************************/
#include <Wire.h>
#include <Adafruit_GFX.h>     // librairie à charger via le gestionnaire de librairie
#include <Adafruit_SSD1306.h> // librairie à charger via le gestionnaire de librairie


#define SCREEN_WIDTH 128 // largeur de l'écran en pixels
#define SCREEN_HEIGHT 64 // hauteur de l'écran en pixels
#define SCREEN_ADRESS 0x3C // adresse de l'esclave 

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);  // ici ecran sans RESET
uint8_t valeur = 0x41;

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADRESS ); // communication lancée avec l'esclave
  delay(2000);
  display.clearDisplay();
  display.setTextColor(WHITE);    // couleur du texte
}

void loop() {
  display.clearDisplay();
  display.setTextSize(2);  // taille de la police 
  display.setCursor(0,0);  //positionnement du texte sur l'écran
  display.print("TEST ECRAN ");
  display.setTextSize(2);
  display.setCursor(0,15);
  display.print("OK ");
  display.setTextSize(2);
  display.setCursor(0, 27);
  display.print("valeur: ");
  display.setTextSize(2);
  display.setCursor(0, 47);
  display.print(String(valeur));  // conversion en chaine de caractères et impression de  valeur
  display.display();
  delay(1000);
}
